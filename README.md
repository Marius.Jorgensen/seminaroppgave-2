# Mitt program


I have made "Snake"! 
Back when cell phones were still a luxury, Snake was the epitome of mobile gaming. It was the most played game until the iPhone came along and swiped its crown. But playing it now is like a blast from the past. And I really hope my version does justice to the original gameplay.

It's simple, but endlessly entertaining. And when you feel you`ve mastered it, crank up the speed for even more challenge. This is just the first edition of "Snake". If I keep improving it, I'll add new boards, a start screen, let you phase through walls, and throw in a 2-player mode. Stay tuned!

The rules of the game are as follows: Use the arrow keys on your keyboard to control the direction of the snake. The snake will move continuously in the current direction until you change it. The goal of the game is to eat as many apples as possible and grow the snake without crashing into any obstacles. When you eat an apple, the snake will grow longer, and you will earn points. If the snake's head collides with its own body or with the walls, the game is over. To pause the game, press the spacebar. To resume the game, press the spacebar again. When the game is over, you can press "R" to restart the game from the beginning. Try to beat your high score and become the ultimate "Snake" champion!


VIDEOLINK:
https://youtu.be/8I2D2UyEegA 


 
