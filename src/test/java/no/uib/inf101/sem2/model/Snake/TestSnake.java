package no.uib.inf101.sem2.model.Snake;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.snake.Snake;

public class TestSnake {

    @Test
    public void moveSnakeTest() {
        Snake snake1 = new Snake(new CellPosition(4, 4));
        Snake snake2 = new Snake(new CellPosition(4, 4));
        snake1.moveSnake(1, 1).moveSnake(1, 1);
        snake2.moveSnake(2, 2);
        CellPosition snake1Pos = snake1.getSnake().pos();
        CellPosition snake2Pos = snake2.getSnake().pos();
        assertEquals(snake1Pos, snake2Pos);
    }

    @Test
    public void moveSnakeTestDirRight() {
        Snake snake = new Snake(new CellPosition(4, 4));
        snake = snake.moveSnake(0, 1);
        CellPosition expected = new CellPosition(4, 5);
        CellPosition actual = snake.getSnake().pos();
        assertEquals(expected, actual);
    }

    @Test
    public void moveSnakeDirLeft() {
        Snake snake = new Snake(new CellPosition(4, 4));
        snake = snake.moveSnake(0, -1);
        CellPosition expected = new CellPosition(4, 3);
        CellPosition actual = snake.getSnake().pos();
        assertEquals(expected, actual);
    }

    @Test
    public void moveSnakeDirUp() {
        Snake snake = new Snake(new CellPosition(4, 4));
        snake = snake.moveSnake(-1, 0);
        CellPosition expected = new CellPosition(3, 4);
        CellPosition actual = snake.getSnake().pos();
        assertEquals(expected, actual);
    }

    @Test
    public void moveSnakeDirDown() {
        Snake snake = new Snake(new CellPosition(4, 4));
        snake = snake.moveSnake(1, 0);
        CellPosition expected = new CellPosition(5, 4);
        CellPosition actual = snake.getSnake().pos();
        assertEquals(expected, actual);
    }
}