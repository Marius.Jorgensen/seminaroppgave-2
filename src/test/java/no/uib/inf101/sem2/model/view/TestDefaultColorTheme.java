package no.uib.inf101.sem2.model.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.awt.Color;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.view.ColorTheme;
import no.uib.inf101.sem2.view.DefaultColorTheme;

public class TestDefaultColorTheme {

    @Test
    public void sanityTestDefaultColorTheme() {
        ColorTheme colors = new DefaultColorTheme();
        assertEquals(Color.ORANGE, colors.getBackgroundColor());
        assertEquals(Color.LIGHT_GRAY, colors.getFrameColor());
        assertEquals(Color.BLACK, colors.getCellColor('-'));
        assertEquals(Color.GREEN, colors.getCellColor('H'));
        assertEquals(Color.RED, colors.getCellColor('A'));
        assertThrows(IllegalArgumentException.class, () -> colors.getCellColor('\n'));
    }
}
