package no.uib.inf101.sem2.model.Snake;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.SnakeBoard;
import no.uib.inf101.sem2.model.SnakeModel;
import no.uib.inf101.sem2.model.snake.Snake;
import no.uib.inf101.sem2.model.snake.SnakeDirectionState;

public class TestSnakeGame {

    // Test if gamestate becomes gameover if it crashes

    @Test
    public void testGameOver() {
        Snake snake = new Snake(new CellPosition(4, 4));
        SnakeBoard board = new SnakeBoard(10, 10);
        SnakeModel model = new SnakeModel(board, snake);
        model.setDirection(SnakeDirectionState.RIGHT);
        for (int i = 0; i < 10; i++) {
        model.clockTick();
        }
        assertEquals(GameState.GAME_OVER, model.getGameState());
    }
   
    @Test
public void testSnakeGrowsByOneAfterEatingApple() {
    Snake snake = new Snake(new CellPosition(4, 4));
    SnakeBoard board = new SnakeBoard(10, 10);
    SnakeModel model = new SnakeModel(board, snake);

    int appleRow = 5; // specify row for apple
    int appleCol = 4; // specify column for apple
    CellPosition applePos = new CellPosition(appleRow, appleCol);
    GridCell<Character> apple = new GridCell<>(applePos, 'A');
    board.set(applePos, 'A'); // use the SnakeBoard object created earlier

    model.setDirection(SnakeDirectionState.DOWN);
    model.clockTick();
    

    // Check that the snake has not grown yet
    // Check that the snake has not grown yet
    int expectedLength = 2;
    int actualLength = model.getSnakeLength();
    assertEquals(expectedLength, actualLength);
}

    


}