package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.snake.Snake;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestSnakeModel {

    private SnakeModel snakeModel;
    private SnakeBoard snakeBoard;
    private Snake snake;

    @BeforeEach
    void setUp() {
        snakeBoard = new SnakeBoard(10, 10);
        snake = new Snake(new CellPosition(4, 4));
        snakeModel = new SnakeModel(snakeBoard, snake);
    }

    @Test
    void testGetGameState() {
        Assertions.assertEquals(GameState.ACTIVE_GAME, snakeModel.getGameState());
    }

    @Test
    void testGetDimension() {
        Assertions.assertEquals(snakeBoard, snakeModel.getDimension());
    }

}
