package no.uib.inf101.sem2;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.SnakeBoard;
import no.uib.inf101.sem2.model.SnakeModel;
import no.uib.inf101.sem2.model.snake.Snake;
import no.uib.inf101.sem2.model.snake.controller.SnakeController;
import no.uib.inf101.sem2.view.SnakeView;

import javax.swing.JFrame;

/**
 * The Main class is the entry point for the Snake game application.
 * It sets up the game board and
 * launches the graphical user interface (GUI).
 */
public class Main {

  /**
   * The main method creates the Snake game objects and launches the GUI.
   * 
   * @param args an array of command-line arguments (not used in this application)
   */
  public static void main(String[] args) {

    Snake snake = new Snake(new CellPosition(10, 10));
    SnakeBoard snakeBoard = new SnakeBoard(25, 25);
    SnakeModel snakeModel = new SnakeModel(snakeBoard, snake);
    SnakeView snakeView = new SnakeView(snakeModel);
    SnakeController snakeController = new SnakeController(snakeModel, snakeView);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Snake");
    frame.setContentPane(snakeView);
    frame.pack();
    frame.setVisible(true);
  }
}
