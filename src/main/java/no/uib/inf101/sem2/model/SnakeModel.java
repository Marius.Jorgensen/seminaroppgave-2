package no.uib.inf101.sem2.model;

import java.util.ArrayList;
import java.util.Random;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.snake.Snake;
import no.uib.inf101.sem2.model.snake.SnakeDirectionState;
import no.uib.inf101.sem2.model.snake.controller.ControllableSnakeModel;
import no.uib.inf101.sem2.view.ViewableSnakeModel;

/**
 * SnakeModel class that implements the ControllableSnakeModel and
 * ViewableSnakeModel interfaces.
 * 
 */
public class SnakeModel implements ViewableSnakeModel, ControllableSnakeModel {

    private GameState gameState = GameState.ACTIVE_GAME;
    private Snake snake;
    private SnakeBoard snakeBoard;
    private SnakeDirectionState direction;
    private SnakeDirectionState oppositeDirection;
    private int snakeLength = 1;
    private ArrayList<CellPosition> snakePos = new ArrayList<>();
    private GridCell<Character> apple;
    private boolean canChangeDirection;
    private int counter;
    private String gameOverMessage;
    private int score;

    /**
     * Constructor for SnakeModel.
     * 
     * @param snakeBoard
     *                   The SnakeBoard
     * @param snake
     *                   The Snake
     */
    public SnakeModel(SnakeBoard snakeBoard, Snake snake) {
        this.snakeBoard = snakeBoard;
        this.snake = snake;
        this.direction = SnakeDirectionState.RIGHT;
        this.oppositeDirection = SnakeDirectionState.LEFT;
        this.snakePos.add(snake.getSnake().pos());
        spawnApple();
        this.canChangeDirection = true;
        this.counter = 0;
        this.gameOverMessage = getRandomGameOverMessage();
    }

    // Spawns apple in a random position on the board if the position is empty
    private void spawnApple() {
        Random random = new Random();
        int x = random.nextInt(snakeBoard.cols());
        int y = random.nextInt(snakeBoard.rows());
        CellPosition applePos = new CellPosition(x, y);
        while (!snakeBoard.get(applePos).equals('-')) {
            x = random.nextInt(snakeBoard.cols());
            y = random.nextInt(snakeBoard.rows());
            applePos = new CellPosition(x, y);
        }
        apple = new GridCell<>(applePos, 'A');
        snakeBoard.set(applePos, 'A');
    }

    private void pointsCalculator() { // Calculates the score
        this.score = counter * 15;
    }

    // This method checks if the snake has eaten an apple.
    // If so, the method increases the length of the snake by one unit ("snakeLength++")
    // and spawns a new apple at a random location on the screen ("spawnApple()"), increases a counter by one unit ("counter++")
    // and calculates the points ("pointsCalculator()").
    // The method compares the position of the snake and the apple by using the "equals()" method of the position objects. 
    //If the positions are equal, it means the snake has eaten the apple.
    private void checkIfAppleEaten() { // Checks if the apple is eaten
        if (snakeBoard.get(snake.getSnake().pos()).equals('A')) {
            snakeLength++;
            spawnApple();
            counter++;
            pointsCalculator();
        }
    }

    // Moves the snake specified by the "direction" parameter of type "SnakeDirectionState".
    // The current head position of the snake is first stored in the "currentHeadPos".
    // If the "gameState" is "ACTIVE_GAME", the snake is moved in the selected direction using a switch-case construct.
    // If the new position of the snake is a legal position (legalPos), the "snake" object is updated with the new position
    // "checkIfAppleEaten()" method is called to check if the snake has eaten an apple, and the "gameState" remains "ACTIVE_GAME".
    // If the new position of the snake is an illegal position, the "gameState" is set to "GAME_OVER".
    // The head of the snake represented by "currentHeadPos" is drawn on "snakeBoard"
    // "currentHeadPos" is added to the "snakePos" list that contains the positions of all parts of the snake, and the "updateSnake()" method is called to update the screen with the new position of the snake.
    private void moveSnake(SnakeDirectionState direction) { //Denne var private, endret for test
        CellPosition currentHeadPos = snake.getSnake().pos();
        if (gameState == GameState.ACTIVE_GAME) {
            Snake snake = switch (direction) {
                case UP -> this.snake.moveSnake(-1, 0);
                case DOWN -> this.snake.moveSnake(1, 0);
                case LEFT -> this.snake.moveSnake(0, -1);
                case RIGHT -> this.snake.moveSnake(0, 1);
            };
            if (legalPos(snake)) {
                this.snake = snake;
                checkIfAppleEaten();
            } else {
                gameState = GameState.GAME_OVER;
            }

            snakeBoard.set(currentHeadPos, 'H');
            snakePos.add(currentHeadPos);
            updateSnake();
        }
    }

    private void updateSnake() {
        while (snakeLength <= snakePos.size()) {
            snakeBoard.set(snakePos.get(0), '-');
            snakePos.remove(0);
        }
    }

    // restartSnake starts the game again when gamestate is gameover.
    // The snakeboard is cleared and the snake is set to the new position.
    // New apple is spawned and the counter is set to 0
    @Override
    public void restartSnake() {
        Snake snake = new Snake(new CellPosition(10, 10));
        SnakeBoard snakeBoard = new SnakeBoard(25, 25);
        this.snake = snake;
        this.snakeBoard = snakeBoard;
        this.direction = SnakeDirectionState.RIGHT;
        this.oppositeDirection = SnakeDirectionState.LEFT;
        this.snakePos.clear();
        this.snakePos.add(snake.getSnake().pos());
        this.snakeLength = 1;
        spawnApple();
        this.canChangeDirection = true;
        this.counter = 0;
        this.gameOverMessage = getRandomGameOverMessage();
        this.gameState = GameState.ACTIVE_GAME;
        this.score = 0;
    }

    @Override
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public GridDimension getDimension() {
        return snakeBoard;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return snakeBoard;
    }

    @Override
    public Iterable<GridCell<Character>> movingSnakeTiles() {
        return snake;
    }

    // This method checks whether a given position is a valid position for the snake to move to by checking if it is on the game board and if the snake head collides with another part of the snake. 
    // It returns true if the position is valid.
    private boolean legalPos(Snake snake) {
        for (GridCell<Character> snakeHead : snake) {

            if (!snakeBoard.positionIsOnGrid(snakeHead.pos())) {
                gameState = GameState.GAME_OVER; // Sett gamestate til GAME_OVER hvis slangehodet er utenfor
                                                 // spillbrettet
                return false;
            }
            if (snakeBoard.get(snakeHead.pos()) == 'H') {
                gameState = GameState.GAME_OVER; // Sett gamestate til GAME_OVER hvis slangehodet kolliderer med en
                                                 // annen del av slangen
                return false;
            }

        }
        return true;
    }

    private String gameOverMessage1 = "Looks like your game plan was a bit of a bust. Time to rethink your strategy!";
    private String gameOverMessage2 = "That was a valiant effort, but unfortunately, the game had other plans for you.";
    private String gameOverMessage3 = "Better luck next time, unless you're planning on repeating the same mistakes...";
    private String gameOverMessage4 = "Game over, but don't be too hard on yourself. After all, Rome wasn't built in a day.";
    private String gameOverMessage5 = "Oof. That was a rough one. Maybe take a break and come back to it later?";

    private String getRandomGameOverMessage() {
        Random random = new Random();
        int index = random.nextInt(5) + 1;
        switch (index) {
            case 1:
                return gameOverMessage1;
            case 2:
                return gameOverMessage2;
            case 3:
                return gameOverMessage3;
            case 4:
                return gameOverMessage4;
            case 5:
                return gameOverMessage5;
            default:
                return "";
        }
    }

    @Override
    public void setDirection(SnakeDirectionState direction) {
        if (canChangeDirection == false)
            return;

        if (direction != oppositeDirection) {
            this.direction = direction;
            switch (direction) {
                case UP:
                    oppositeDirection = SnakeDirectionState.DOWN;
                    break;
                case DOWN:
                    oppositeDirection = SnakeDirectionState.UP;
                    break;
                case LEFT:
                    oppositeDirection = SnakeDirectionState.RIGHT;
                    break;
                case RIGHT:
                    oppositeDirection = SnakeDirectionState.LEFT;
                    break;
                default:
                    break;
            }
        }
        canChangeDirection = false;

    }

    @Override
    public void clockTick() {
        if (gameState == GameState.ACTIVE_GAME) {
            moveSnake(direction);
        }
        canChangeDirection = true;
    }

    @Override
    public int getDelay() {
        return 136;
        // Fast speed = 136 ref https://google-snake.fandom.com/wiki/Fast_Speed
    }

    @Override
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    @Override
    public String getGameOverMessage() {
        return gameOverMessage;
    }

    @Override
    public int getScore() {
        return score;

    }

    public Grid<Character> getSnakeBoard() {
        return null;
    }

    public int getSnakeLength() {
        return snakeLength;
    }


}