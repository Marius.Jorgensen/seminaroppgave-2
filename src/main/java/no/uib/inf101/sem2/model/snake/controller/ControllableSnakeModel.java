package no.uib.inf101.sem2.model.snake.controller;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.snake.SnakeDirectionState;

/**
 * The interface for the snake model.
 * 
 * @param setDirection Sets the direction of the snake
 * @param clockTick    Updates the snake
 * @param getDelay     Gets the delay of the snake
 * @param setGameState Sets the game state
 * @param getGameState Gets the game state
 * @param restartSnake Restarts the snake
 */
public interface ControllableSnakeModel {
    void setDirection(SnakeDirectionState direction);

    void clockTick();

    int getDelay();

    void setGameState(GameState gameState);

    GameState getGameState();

    void restartSnake();
}
