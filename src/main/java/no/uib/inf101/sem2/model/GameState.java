package no.uib.inf101.sem2.model;

/**
 * The state of the game.
 * 
 * @param ACTIVE_GAME The game is active
 * @param PAUSED      The game is paused
 * @param GAME_OVER   The game is over
 */

public enum GameState {
    ACTIVE_GAME, PAUSED, GAME_OVER;
}
