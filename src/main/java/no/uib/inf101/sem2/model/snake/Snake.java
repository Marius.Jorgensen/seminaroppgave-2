package no.uib.inf101.sem2.model.snake;

import java.util.ArrayList;
import java.util.Iterator;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

/**
 * A snake is a list of grid cells.
 * 
 * @param head the head of the snake
 */
public class Snake implements Iterable<GridCell<Character>> {

    private GridCell<Character> head;

    /**
     * Create a new snake that starts with head.
     * 
     * @param firstHeadPosition the position of the head
     */
    public Snake(CellPosition firstHeadPosition) {
        head = new GridCell<Character>(firstHeadPosition, 'H');
    }

    /**
     * Get the head of the snake.
     * 
     * @return the head of the snake
     */
    public GridCell<Character> getSnake() {
        return this.head;
    }

    @Override
    public Iterator<GridCell<Character>> iterator() {
        ArrayList<GridCell<Character>> snake = new ArrayList<>();
        // Add the head in the middle of the board 
        for (int i = 0; i < 5; i++) {
            snake.add(head);
        }
        return snake.iterator();
    }

    /**
     * Move the snake in the given direction.
     * 
     * @param deltaRow the change in row
     * @param deltaCol the change in column
     * @return a new snake that has moved
     */
    public Snake moveSnake(int deltaRow, int deltaCol) {
        int newRow = head.pos().row() + deltaRow;
        int newCol = head.pos().col() + deltaCol;
        return new Snake(new CellPosition(newRow, newCol));
    }

    public Object getGameState() {
        return null;
    }

}
