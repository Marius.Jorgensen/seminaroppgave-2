package no.uib.inf101.sem2.model.snake;

/**
 * The direction of the snake.
 * 
 * @param DOWN  The snake is moving down
 * @param UP    The snake is moving up
 * @param LEFT  The snake is moving left
 * @param RIGHT The snake is moving right
 */
public enum SnakeDirectionState {
    DOWN, UP, LEFT, RIGHT,
}
