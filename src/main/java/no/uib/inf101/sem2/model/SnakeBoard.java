package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;

/**
 * The board of the snake game.
 * 
 * @param rows
 * @param cols
 * @param snakeBoardTile
 * @param row
 * @param col
 * @param prettystringboard
 * @param head
 * @param c
 */
public class SnakeBoard extends Grid<Character> {

    /**
     * 
     * @param rows
     * @param cols
     *             SnakeBoard constructor that creates a new grid with the given
     *             number of rows and columns.
     */
    public SnakeBoard(int rows, int cols) {
        super(rows, cols);
        for (GridCell<Character> snakeBoardTile : this) {
            this.set(snakeBoardTile.pos(), '-');
        }
    }

    /**
     * 
     * @param head
     * @param c
     *             prettyString method that returns a string representation of the
     *             board.
     */
    public String prettyString() { //Fra tetris
        String prettystringboard = "";
        for (int row = 0; row < this.rows(); row++) {
            for (int col = 0; col < this.cols(); col++) {
                prettystringboard += this.get(new CellPosition(row, col));

            }
            if (row < this.rows() - 1) {
                prettystringboard += "\n";
            }

        }
        return prettystringboard;
    }

    /**
     * 
     * @param head
     * @param c
     *             setCell method that sets the character at the given position.
     */
    public void setCell(CellPosition head, char c) {
    }

}
