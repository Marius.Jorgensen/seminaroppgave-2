package no.uib.inf101.sem2.model.snake.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Timer;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.snake.SnakeDirectionState;
import no.uib.inf101.sem2.view.SnakeView;

/**
 * The controller for the Snake game.
 * 
 * @param model The model for the Snake game
 * @param view  The view for the Snake game
 */
public class SnakeController implements java.awt.event.KeyListener {

    private SnakeView view;
    private ControllableSnakeModel model;
    private Timer timer;

    /**
     * Create a new SnakeController.
     * 
     * @param model The model for the Snake game
     * @param view  The view for the Snake game
     */
    public SnakeController(ControllableSnakeModel model, SnakeView view) {
        this.model = model;
        this.view = view;
        view.addKeyListener(this);
        this.timer = new Timer(model.getDelay(), this::clockTick);
        timer.start();
    }

    private void clockTick(ActionEvent action) {
        this.model.clockTick();
        view.repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            // Left arrow was pressed
            model.setDirection(SnakeDirectionState.LEFT);
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            // Right arrow was pressed
            model.setDirection(SnakeDirectionState.RIGHT);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            // Down arrow was pressed
            model.setDirection(SnakeDirectionState.DOWN);
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            // Up arrow was pressed
            model.setDirection(SnakeDirectionState.UP);
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (model.getGameState() == GameState.PAUSED) {
                model.setGameState(GameState.ACTIVE_GAME);
            } else if (model.getGameState() == GameState.ACTIVE_GAME) {
                model.setGameState(GameState.PAUSED);
            }
        } else if (e.getKeyCode() == KeyEvent.VK_R) {
            if (model.getGameState() == GameState.GAME_OVER) {
                model.restartSnake();
                model.setGameState(GameState.ACTIVE_GAME);
            }
        }
        view.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
