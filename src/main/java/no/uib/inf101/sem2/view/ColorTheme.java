package no.uib.inf101.sem2.view;

import java.awt.Color;

/**
 * 
 * Interface ColorTheme with parameter Color.
 * 
 * @param getGameOverColor
 * @param getGameOverTextColor
 * @param getCellColor
 * @param getFrameColor
 * @param getBackgroundColor
 */
public interface ColorTheme {

    /**
     * getGameOverColor returns color.(0, 0, 0, 128) For the rectangle/board/grid
     * 
     * @return
     */
    Color getGameOverColor();

    /**
     * getGameOverTextColor returns color. For the text in the rectangle/board/grid
     * 
     * @return
     */
    Color getGameOverTextColor();

    /**
     * getCellColor returns color based on the character its given.
     * Return value can`t be null!
     * 
     * @param symbol
     * @return
     */
    Color getCellColor(char symbol);

    /**
     * getFrameColor returns color. Return value can`t be null!
     * 
     * @return
     */
    Color getFrameColor();

    /**
     * getBackgroundColor : Can be null
     * 
     * @return
     */
    Color getBackgroundColor();

    Color getPauseColor();

    Color getScoreColor();

}
