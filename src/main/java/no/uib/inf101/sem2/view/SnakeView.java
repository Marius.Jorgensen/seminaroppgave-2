package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.GameState;

/**
 * 
 * Class SnakeView with parameter JPanel.
 * 
 * @param OUTERMARGIN
 * @param INNERMARGIN
 * @param tileSize
 * @param preferredWidth
 * @param preferredHeight
 * @param colorTheme
 * @param model
 * @param drawCells
 * @param drawGame
 */
public class SnakeView extends JPanel {

    // Since these values are not associated with any particular instance of the
    // class, making them static will save memory and improve performance.
    // Therefore, static makes sense in this context.
    private static final double OUTERMARGIN = 30;
    private static final double INNERMARGIN = 0;
    private static final double tileSize = 25;
    private double preferredWidth;
    private double preferredHeight;
    private ColorTheme colorTheme;
    private ViewableSnakeModel model;

    /**
     * SnakeView retrieves number of rows and cols on grid
     * and which color it should have.
     */
    public SnakeView(ViewableSnakeModel model) {
        this.model = model;
        this.preferredWidth = model.getDimension().cols() * tileSize + 2 * OUTERMARGIN;
        this.preferredHeight = model.getDimension().rows() * tileSize + 2 * OUTERMARGIN;
        this.setFocusable(true); // Hentet fra SampleView.java

        this.setPreferredSize(new Dimension((int) preferredWidth, (int) preferredHeight));

        this.colorTheme = new DefaultColorTheme();
        this.setBackground(colorTheme.getBackgroundColor());

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        drawGame(g2d);
    }

    // Makes sense to have static.
    // Draws the cells on the board.
    private static void drawCells(Graphics2D canvasToBeDrawn, Iterable<GridCell<Character>> collectionOfCells,
            CellPositionToPixelConverter translateFromPosToRectangle, ColorTheme toDecideColor) {
        for (GridCell<Character> gridChar : collectionOfCells) {
            Rectangle2D tile = translateFromPosToRectangle.getBoundsForCell(gridChar.pos());
            Color colorThemeColor = toDecideColor.getCellColor(gridChar.value()); // Variabel for farge
            canvasToBeDrawn.setColor(colorThemeColor);
            canvasToBeDrawn.fill(tile);
        }
    }

    // Method drawGame with parameter Graphics2D.
    // Draws the game: frame + cells, the score and the game over screen.
    private void drawGame(Graphics2D gr2d) {
        double width = this.getWidth() - 2 * OUTERMARGIN;
        double height = this.getHeight() - 2 * OUTERMARGIN;
        Rectangle2D frameRectangle = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
        gr2d.setColor(colorTheme.getFrameColor());
        gr2d.fill(frameRectangle);
        CellPositionToPixelConverter cellConverter = new CellPositionToPixelConverter(frameRectangle,
                model.getDimension(),
                INNERMARGIN);
        drawCells(gr2d, model.getTilesOnBoard(), cellConverter, colorTheme);
        drawCells(gr2d, model.movingSnakeTiles(), cellConverter, colorTheme);
        // Draw points / score
        gr2d.setColor(((ColorTheme) colorTheme).getScoreColor());
        gr2d.setFont(new Font("Helvetica", Font.PLAIN, 20));
        Inf101Graphics.drawCenteredString(gr2d, "Score: " + model.getScore(), width / 2, OUTERMARGIN / 2);
        if (model.getGameState() == GameState.GAME_OVER) {
            drawGameOver(gr2d);
        }
        if (model.getGameState() == GameState.PAUSED) {
            drawPauseScreen(gr2d);
        }
    }

    // Method drawGameOver draws the pause screen.
    private void drawPauseScreen(Graphics2D gr2d) {
        double width = this.getWidth();
        double height = this.getHeight();
        Rectangle2D pause = new Rectangle2D.Double(0, 0, width, height);
        gr2d.setColor(colorTheme.getPauseColor());
        gr2d.fill(pause);
        gr2d.setColor(colorTheme.getGameOverTextColor());
        gr2d.setFont(new Font("Helvetica", Font.PLAIN, 45));
        Inf101Graphics.drawCenteredString(gr2d, "Snake is resting", pause);
        gr2d.setFont(new Font("Helvetica", Font.PLAIN, 14));
        Inf101Graphics.drawCenteredString(gr2d,
                "Game over man! Just kidding, it's just paused. Take a deep breath and get back in there.",
                new Rectangle2D.Double(0, height / 2 - 30, width, height / 2));
    }


    // Method drawGameOver draws the game over screen.
    private void drawGameOver(Graphics2D gr2d) {
        double width = this.getWidth();
        double height = this.getHeight();
        Rectangle2D gameOverRec = new Rectangle2D.Double(0, 0, width, height);
        Rectangle2D restartMessage = new Rectangle2D.Double(0, height / 2.5, width, height / 2.5);
        gr2d.setColor(colorTheme.getGameOverColor());
        gr2d.fill(gameOverRec);
        gr2d.setColor(colorTheme.getGameOverTextColor());
        gr2d.setFont(new Font("Helvetica", Font.PLAIN, 45));
        Inf101Graphics.drawCenteredString(gr2d, "GAME OVER", gameOverRec);
        gr2d.setFont(new Font("Helvetica", Font.PLAIN, 20));
        Inf101Graphics.drawCenteredString(gr2d, "Press R to restart", restartMessage);

        String message = model.getGameOverMessage();
        drawGameOverAlt(gr2d, width, height, message);
    }

    // Draws the game over screen with randomized messages.
    private void drawGameOverAlt(Graphics2D gr2d, double width, double height, String message) {
        gr2d.setFont(new Font("Helvetica", Font.PLAIN, 14));
        Inf101Graphics.drawCenteredString(gr2d, message, new Rectangle2D.Double(0, height / 2 - 30, width, height / 2));
    }

}
