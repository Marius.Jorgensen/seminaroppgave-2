package no.uib.inf101.sem2.view;

import java.awt.Color;

/**
 * 
 * Class DefaultColorTheme implements ColorTheme.
 * 
 * @param getGameOverColor
 * @param getGameOverTextColor
 * @param getCellColor
 * @param getFrameColor
 * @param getBackgroundColor
 */
public class DefaultColorTheme implements ColorTheme {

    @Override
    public Color getCellColor(char symbol) {
        Color color = switch (symbol) {
            case 'H' -> Color.GREEN;
            case '-' -> Color.BLACK;
            case 'A' -> Color.RED;
            default -> throw new IllegalArgumentException(
                    "No available color for '" + symbol + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return Color.LIGHT_GRAY;
    }

    @Override
    public Color getBackgroundColor() {
        return Color.ORANGE;
    }

    @Override
    public Color getGameOverColor() {
        return new Color(0, 0, 0, 196); // Justert litt opp fra 128 = 50% transparency
    }

    @Override
    public Color getPauseColor() {
        return new Color(0, 0, 0, 196); // Justert litt opp fra 128 = 50% transparency
    }

    @Override
    public Color getGameOverTextColor() {
        return Color.WHITE;
    }

    @Override
    public Color getScoreColor() {
        return Color.BLACK;
    }
}
