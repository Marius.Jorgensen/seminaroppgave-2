package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameState;

/**
 * The ViewableSnakeModel interface provides methods for getting information
 * about the state of the Snake game,
 * including the current game state, board dimensions, tile positions, and
 * score.
 */
public interface ViewableSnakeModel {

    /**
     * Returns the current GameState of the Snake game, which tells us whether the
     * game is active or GameOver.
     *
     * @return the current GameState object
     */
    GameState getGameState();

    /**
     * Returns the GridDimension object representing the number of columns and rows
     * on the game board.
     *
     * @return the GridDimension object representing the dimensions of the game
     *         board
     */
    GridDimension getDimension();

    /**
     * Returns an Iterable of GridCell objects representing all positions on the
     * board with the same symbol.
     *
     * @return an Iterable of GridCell objects representing all positions on the
     *         board with the same symbol
     */
    Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * Returns an Iterable of GridCell objects representing all positions of the
     * moving snake tiles.
     *
     * @return an Iterable of GridCell objects representing all positions of the
     *         moving snake tiles
     */
    Iterable<GridCell<Character>> movingSnakeTiles();

    /**
     * Returns a string message indicating the game over condition, if the game is
     * over.
     *
     * @return a string message indicating the game over condition, if the game is
     *         over; otherwise, null
     */
    String getGameOverMessage();

    /**
     * Returns the current score of the game.
     *
     * @return the current score of the game
     */
    int getScore();

}
